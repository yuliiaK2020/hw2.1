package classWorkATests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ATConverter {
    @Test
    public void  chekcMyConverter_toEng (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement element = driver.findElement(By.id("btnSettings"));
        element.click();
        WebElement languageList = driver.findElement(By.id("languageList"));
        languageList.click();
        WebElement language = driver.findElement(By.id("EN"));
        language.click();
        WebElement btn = driver.findElement(By.id("btnSaveSettings"));
        btn.click();
        WebElement result = driver.findElement(By.id("lblSettings"));
        result.getText();
        String actual = result.getText();;
        Assert.assertEquals("Choose language", actual);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_toRU (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement element = driver.findElement(By.id("btnSettings"));
        element.click();
        WebElement languageList = driver.findElement(By.id("languageList"));
        languageList.click();
        WebElement language = driver.findElement(By.id("RU"));
        language.click();
        WebElement btn = driver.findElement(By.id("btnSaveSettings"));
        btn.click();
        WebElement result = driver.findElement(By.id("lblSettings"));
        result.getText();
        String actual = result.getText();;
        Assert.assertEquals("Выберите язык", actual);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_toAR (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement element = driver.findElement(By.id("btnSettings"));
        element.click();
        WebElement languageList = driver.findElement(By.id("languageList"));
        languageList.click();
        WebElement language = driver.findElement(By.id("AR"));
        language.click();
        WebElement btn = driver.findElement(By.id("btnSaveSettings"));
        btn.click();
        WebElement result = driver.findElement(By.id("lblSettings"));
        result.getText();
        String actual = result.getText();;
        Assert.assertEquals("اختر اللغة", actual);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_VtoV (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='versts']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("10");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='versts']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(10.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_FtoF (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='foots']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("10");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='foots']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(10.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_METRStoMETRS (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='meters']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("10");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='meters']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(10.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_YtoY (){
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
    ChromeDriver driver = new ChromeDriver();
    driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

    WebElement selectFrom = driver.findElement(By.id("covertFrom"));
    selectFrom.click();
    selectFrom.findElement(new By.ByCssSelector("[value='yards']")).click(); //from
    WebElement inputField=driver.findElement(new By.ById("inputNumber"));
    inputField.sendKeys("10");
    WebElement selectTo = driver.findElement(By.id("covertTo"));
    selectTo.click();
    selectTo.findElement(new By.ByCssSelector("[value='yards']")).click(); //to
    WebElement element3 = driver.findElement(By.id("btnConvert"));
    element3.click();
    WebElement result = driver.findElement(By.id("inputResult"));
    result.getAttribute("value");

    double actual=Double.parseDouble(result.getAttribute("value"));
    Assert.assertEquals(10.0, actual, 0);
    driver.quit();
    }
    @Test
    public void  chekcMyConverter_MilestoMiles (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='miles']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("10");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='miles']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(10.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_VtoMeters (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='versts']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='meters']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(106680.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_VtoF (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='versts']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='foots']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(350000.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_VtoMiles (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='versts']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='miles']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(66.2879, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_VtoY (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='versts']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='yards']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(116666.67, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MeterstoV (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='meters']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='versts']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(0.0937, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MeterstoF (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='meters']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='foots']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(328.1, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MeterstoMiles (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='meters']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='miles']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(0.0621, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MeterstoY (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='meters']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='yards']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(109.3613, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_FtoV (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='foots']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='versts']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(0.0286, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_FtoMeters (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='foots']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='meters']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(30.48, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_FtoMiles (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='foots']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='miles']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(0.0189, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_FtoY (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='foots']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='yards']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(33.3333, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_YtoV (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='yards']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='versts']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(0.0857, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_YtoMeters (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='yards']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='meters']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(91.44, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_YtoMiles (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='yards']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='miles']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(0.0568, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_YtoF (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='yards']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='foots']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(300.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MilestoV (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");

        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='miles']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='versts']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");

        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(150.8571, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MilestoMeters (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='miles']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='meters']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(160934.4, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MilestoY (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='miles']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='yards']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(176000.0, actual, 0);
        driver.quit();
    }
    @Test
    public void  chekcMyConverter_MilestoF (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/converterLength/index.html");
        WebElement selectFrom = driver.findElement(By.id("covertFrom"));
        selectFrom.click();
        selectFrom.findElement(new By.ByCssSelector("[value='miles']")).click(); //from
        WebElement inputField=driver.findElement(new By.ById("inputNumber"));
        inputField.sendKeys("100");
        WebElement selectTo = driver.findElement(By.id("covertTo"));
        selectTo.click();
        selectTo.findElement(new By.ByCssSelector("[value='foots']")).click(); //to
        WebElement element3 = driver.findElement(By.id("btnConvert"));
        element3.click();
        WebElement result = driver.findElement(By.id("inputResult"));
        result.getAttribute("value");
        double actual=Double.parseDouble(result.getAttribute("value"));
        Assert.assertEquals(528000.0, actual, 0);
        driver.quit();
    }

}
