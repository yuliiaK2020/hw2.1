package classWorkATests;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ATCalc {
    @Test
    public void  chekcMyCalc_5pl6pl0 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("five"));
        num1.click();
        WebElement operation1 = driver.findElement(By.id("add"));
        operation1.click();
        WebElement num2 = driver.findElement(By.id("six"));
        num2.click();
        WebElement operation2 = driver.findElement(By.id("add"));
        operation2.click();
        WebElement num3 = driver.findElement(By.id("zero"));
        num3.click();
        WebElement elment1 = driver.findElement(By.id("equals"));
        elment1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();

        int actual = Integer.parseInt(result.getText());
        Assert.assertEquals(11, actual);
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_25div5 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("two"));
        num1.click();
        WebElement num2 = driver.findElement(By.id("five"));
        num2.click();
        WebElement operation1 = driver.findElement(By.id("divide"));
        operation1.click();
        WebElement num3 = driver.findElement(By.id("five"));
        num2.click();

        WebElement elment1 = driver.findElement(By.id("equals"));
        elment1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();

        int actual = Integer.parseInt(result.getText());
        Assert.assertEquals(5, actual);
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_5_point2pl9point3 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("five"));
        num1.click();
        WebElement elem = driver.findElement(By.id("decimal"));
        elem.click();
        WebElement num2 = driver.findElement(By.id("two"));
        num2.click();
        WebElement operation2 = driver.findElement(By.id("add"));
        operation2.click();
        WebElement num3 = driver.findElement(By.id("nine"));
        num3.click();
        WebElement elem1 = driver.findElement(By.id("decimal"));
        elem1.click();
        WebElement num4 = driver.findElement(By.id("three"));
        num4.click();

        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();

        WebElement result1 = driver.findElement(By.id("display"));
        result1.getText();

        Assert.assertEquals("14.5", result1.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_6div6 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("six"));
        num1.click();
        WebElement operation1 = driver.findElement(By.id("divide"));
        operation1.click();
        WebElement num2 = driver.findElement(By.id("six"));
        num2.click();

        WebElement elment1 = driver.findElement(By.id("equals"));
        elment1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();

        int actual = Integer.parseInt(result.getText());
        Assert.assertEquals(1, actual);
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_9div_0 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("nine"));
        num1.click();
        WebElement operation1 = driver.findElement(By.id("divide"));
        operation1.click();
        WebElement num2 = driver.findElement(By.id("zero"));
        num2.click();

        WebElement elment1 = driver.findElement(By.id("equals"));
        elment1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();


        Assert.assertEquals("Infinity", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_11mult7 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        WebElement elem = driver.findElement(By.id("decimal"));
        WebElement num2 = driver.findElement(By.id("one"));
        num2.click();
        WebElement operation1 = driver.findElement(By.id("multiply"));
        operation1.click();
        WebElement num3 = driver.findElement(By.id("seven"));
        num3.click();

        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();

        int actual = Integer.parseInt(result.getText());
        Assert.assertEquals(77, actual);
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_0mult10point1 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("zero"));
        num1.click();
        WebElement elem = driver.findElement(By.id("multiply"));
        elem.click();
        WebElement num2 = driver.findElement(By.id("one"));
        num2.click();
        WebElement num3 = driver.findElement(By.id("zero"));
        num3.click();
        WebElement elem1 = driver.findElement(By.id("decimal"));
        elem1.click();
        WebElement num4 = driver.findElement(By.id("one"));
        num4.click();

        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();

        WebElement result = driver.findElement(By.id("display"));
        result.getText();


        Assert.assertEquals("0",result.getText());
        driver.quit();

    }
    @Test
    public void  chekcMyCalc_neg1mult1 (){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        WebElement elem = driver.findElement(By.id("neg"));
        elem.click();
        WebElement elem1 = driver.findElement(By.id("add"));
        elem1.click();
        WebElement num4 = driver.findElement(By.id("one"));
        num4.click();

        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();

        WebElement result = driver.findElement(By.id("display"));
        result.getText();


        Assert.assertEquals("0",result.getText());
        driver.quit();

    }
    @Test
    public void  chekcMyCalc_neg5_decimal5div5 () {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("five"));
        num1.click();
        WebElement element = driver.findElement(By.id("decimal"));
        element.click();
        WebElement num = driver.findElement(By.id("five"));
        num.click();
        WebElement elem = driver.findElement(By.id("neg"));
        elem.click();
        WebElement elem1 = driver.findElement(By.id("divide"));
        elem1.click();
        WebElement num4 = driver.findElement(By.id("five"));
        num4.click();

        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();

        WebElement result = driver.findElement(By.id("display"));
        result.getText();


        Assert.assertEquals("-1.1", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_neg5_decimal5div_neg5 () {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("five"));
        num1.click();
        WebElement element = driver.findElement(By.id("decimal"));
        element.click();
        num1.click();
        WebElement elem = driver.findElement(By.id("neg"));
        elem.click();
        WebElement elem1 = driver.findElement(By.id("divide"));
        elem1.click();
        num1.click();
        elem.click();
        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();

        WebElement result = driver.findElement(By.id("display"));
        result.getText();


        Assert.assertEquals("1.1", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_neg1_minuss5 () {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        WebElement elem = driver.findElement(By.id("neg"));
        elem.click();
        WebElement elem1 = driver.findElement(By.id("subtract"));
        elem1.click();
        WebElement num2 = driver.findElement(By.id("five"));
        num2.click();
        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("-6", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_1_sin () {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");

        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        WebElement elem1 = driver.findElement(By.id("sin"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("0.017452406437283512819", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_1_cos () {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        WebElement elem1 = driver.findElement(By.id("cos"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("0.99984769515639123916", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_1_del () {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        num1.click();
        num1.click();
        WebElement elem1 = driver.findElement(By.id("clear"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("0", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_5_square() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("five"));
        num1.click();
        WebElement elem1 = driver.findElement(By.id("square"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("25", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_5_cube() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("five"));
        num1.click();
        WebElement elem1 = driver.findElement(By.id("cube"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("125", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_5_x() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("five"));
        num1.click();
        WebElement elem1 = driver.findElement(By.id("pow"));
        elem1.click();
        num1.click();
        WebElement element1 = driver.findElement(By.id("equals"));
        element1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("3125", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_9_() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("nine"));
        num1.click();
        WebElement elem1 = driver.findElement(By.id("sqrt"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("3", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_8_() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("eight"));
        num1.click();
        WebElement elem1 = driver.findElement(By.id("cbrt"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("2", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_10fact() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        WebElement num2 = driver.findElement(By.id("zero"));
        num2.click();
        WebElement elem1 = driver.findElement(By.id("factorial"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("3628800", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_10percent() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("one"));
        num1.click();
        WebElement num2 = driver.findElement(By.id("zero"));
        num2.click();
        num2.click();
        WebElement elem1 = driver.findElement(By.id("percent"));
        elem1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("1", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_PI() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("PI"));
        num1.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("3.1415926535897932385", result.getText());
        driver.quit();
    }
    @Test
    public void  chekcMyCalc_4log() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\chromedriver\\chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get("http://34.90.36.71/apps/shpitalniy/scientificCalculator/index.html");
        WebElement num1 = driver.findElement(By.id("four"));
        num1.click();
        WebElement elem = driver.findElement(By.id("log"));
        elem.click();
        WebElement result = driver.findElement(By.id("display"));
        result.getText();
        Assert.assertEquals("0.60205999132796239043", result.getText());
        driver.quit();
    }
}
